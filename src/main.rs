use std::{fmt::Display, fs::File, io::Read};

fn main() {
    let f = std::env::args().collect::<Vec<_>>();
    let mut prog = Program::new(f[1].as_str());
    prog.run();
}

#[inline]
pub fn b2i(x: bool) -> i64 {
    match x {
        false => 0,
        true => 1,
    }
}

#[derive(Debug, Clone)]
enum Value {
    Int(i64),
    List(Vec<Value>),
}

impl Display for Value {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Int(x) => f.write_fmt(format_args!("{}", x)),
            Self::List(x) => f.write_fmt(format_args!(
                "[{}]",
                x.iter()
                    .map(|e| format!("{}", e))
                    .collect::<Vec<_>>()
                    .join(", ")
            )),
        }
    }
}

impl Value {
    pub fn iter<'a>(&'a self) -> impl Iterator<Item = &'a Value> {
        match self {
            Value::Int(_) => std::slice::from_ref(self).into_iter().cycle(),
            Value::List(v) => v.as_slice().into_iter().cycle(),
        }
    }
    pub fn len(&self) -> usize {
        match &self {
            Value::Int(_) => 1,
            Value::List(v) => v.len(),
        }
    }

    pub fn binary_op(a: &Self, b: &Self, op: fn(x: i64, y: i64) -> i64) -> Self {
        match (a, b) {
            (Value::Int(x), Value::Int(y)) => Value::Int(op(*x, *y)),
            _ => Value::List(
                a.iter()
                    .zip(b.iter())
                    .map(|(x, y)| Value::binary_op(&x, &y, op))
                    .take(a.len().max(b.len()))
                    .collect::<Vec<_>>(),
            ),
        }
    }

    pub fn numbers(&self) -> Vec<i64> {
        match self {
            Value::Int(i) => vec![*i],
            Value::List(l) => l.iter().map(|e| e.numbers()).fold(vec![], |mut a, mut v| {
                a.append(&mut v);
                a
            }),
        }
    }
}

struct Program {
    stack: Vec<Value>,
    offload_stack: Vec<Value>,
    code: String,
    pos: usize,
}

impl Program {
    pub fn new(filename: &str) -> Self {
        let mut source_file = File::open(filename).unwrap();
        let mut code = String::new();
        source_file.read_to_string(&mut code).unwrap();
        Self {
            stack: vec![],
            offload_stack: vec![],
            code,
            pos: 0,
        }
    }

    pub fn run(&mut self) {
        let l = self.code.chars().collect::<Vec<_>>().len();
        while self.pos < l {
            self.step()
        }
    }

    pub fn step(&mut self) {
        let g = self.code.chars().nth(self.pos).unwrap(); // efficiency!
        match g {
            '\n' | ' ' | '\t' => (),

            // ----------- numbers
            '0' => self.stack.push(Value::Int(0)),
            '1' => self.stack.push(Value::Int(1)),
            '2' => self.stack.push(Value::Int(2)),
            '3' => self.stack.push(Value::Int(3)),
            '4' => self.stack.push(Value::Int(4)),
            '5' => self.stack.push(Value::Int(5)),
            '6' => self.stack.push(Value::Int(6)),
            '7' => self.stack.push(Value::Int(7)),
            '8' => self.stack.push(Value::Int(8)),
            '9' => self.stack.push(Value::Int(9)),

            // ----------- binary ops
            '+' => self.binary_op(|a, b| a + b),
            '-' => self.binary_op(|a, b| a - b),
            '*' => self.binary_op(|a, b| a * b),
            '/' => self.binary_op(|a, b| a / b),
            '%' => self.binary_op(|a, b| a % b),
            '<' => self.binary_op(|a, b| b2i(a < b)),
            '>' => self.binary_op(|a, b| b2i(a > b)),
            '=' => self.binary_op(|a, b| b2i(a == b)),
            '!' => self.binary_op(|a, b| b2i(a != b)),

            // ----------- misc
            '"' => self.duplicate(),
            '~' => self.swap(),
            '@' => self.swap_three(),
            '\'' => {
                self.stack.pop().unwrap();
            }
            '»' => self.offload_stack.push(self.stack.pop().unwrap()),
            '«' => self.stack.push(self.offload_stack.pop().unwrap()),

            // ----------- lists
            '#' => self.stack.push(Value::List(vec![])),
            ';' => {
                let x = self.stack.pop().unwrap();
                match self.stack.pop().unwrap() {
                    Value::Int(_) => panic!("wtf"),
                    Value::List(mut v) => {
                        v.push(x);
                        self.stack.push(Value::List(v));
                    }
                }
            }
            ',' => match self.stack.pop().unwrap() {
                Value::Int(_) => panic!("wtf"),
                Value::List(mut v) => {
                    let x = v.pop().unwrap_or(Value::Int(0));
                    self.stack.push(Value::List(v));
                    self.stack.push(x);
                }
            },

            // ----------- control flow
            '{' => self.stack.push(Value::Int(self.pos as i64)),
            '}' => match self.stack.pop().unwrap() {
                Value::Int(x) => self.pos = x as usize,
                _ => panic!("can only jump to numbers"),
            },

            '(' => {
                let mut b = 1;
                let mut p = self.pos;
                while b > 0 {
                    p += 1;
                    let c = self.code.chars().nth(p).unwrap();
                    match c {
                        '(' => b += 1,
                        ')' => b -= 1,
                        _ => (),
                    }
                }
                self.stack.push(Value::Int(p as i64))
            }
            ')' => (),

            // ----------- io
            'p' => println!("{}", self.stack.pop().unwrap()),
            'P' => println!(
                "{}",
                String::from_utf8(
                    self.stack
                        .pop()
                        .unwrap()
                        .numbers()
                        .into_iter()
                        .map(|e| e as u8)
                        .collect()
                )
                .unwrap()
            ),
            '¤' => println!("{:?}", self.stack),

            _ => eprintln!("unknown char {:?} at index {}", g, self.pos),
        }
        match g {
            '}' => (),
            _ => self.pos += 1,
        }
    }

    pub fn binary_op(&mut self, op: fn(i64, i64) -> i64) {
        let a = self.stack.pop().unwrap();
        let b = self.stack.pop().unwrap();
        self.stack.push(Value::binary_op(&a, &b, op));
    }

    pub fn duplicate(&mut self) {
        let x = self.stack.pop().unwrap();
        self.stack.push(x.clone());
        self.stack.push(x);
    }

    pub fn swap(&mut self) {
        let x = self.stack.pop().unwrap();
        let y = self.stack.pop().unwrap();
        self.stack.push(x);
        self.stack.push(y);
    }
    pub fn swap_three(&mut self) {
        let x = self.stack.pop().unwrap();
        let y = self.stack.pop().unwrap();
        let z = self.stack.pop().unwrap();
        self.stack.push(x);
        self.stack.push(y);
        self.stack.push(z);
    }
}
